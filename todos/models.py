from tkinter import CASCADE
from django.db import models

# Create your models here.
class TodoList(models.Model):
    name = models.CharField(max_length=100)
    created_on = models.DateTimeField(auto_now=False)

    def __str__(self):
        self = self.name


class TodoItem(models.Model):
    task = models.CharField(max_length=100)
    due_date = models.DateTimeField()
    is_completed = models.DateTimeField()
    list = models.ForeignKey(TodoList, related_name="items", on_delete=CASCADE)

    def __str__(self):
        self = self.task
